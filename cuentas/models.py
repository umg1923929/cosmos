from django.db import models
from django.contrib.auth.models import AbstractUser, User

# Create your models here.

class User(AbstractUser):
    pass
    
    def __str__(self):
        return self.username

class Tarea(models.Model):
    id= models.AutoField(primary_key=True) #ide autoincrementable
    titulo = models.CharField(max_length=100, verbose_name='Título')
    imagen = models.ImageField(upload_to='imagenes/', verbose_name='Imagen', null=False, blank=True)
    descripcion = models.TextField(verbose_name='Descripción', null=False)
    creado = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    fechacompletada = models.DateTimeField(null=True, blank=True)
    valor = models.BooleanField(default=False) # 
    usuario = models.ForeignKey(User, on_delete=models.CASCADE,  blank=True)#null=True,

    def __str__(self):
        fila = "Titulo: " + self.titulo + "-" + "Descripción: " + self.descripcion + "- por " + self.usuario.username
        return fila 
    
    def delete(self,  using=None, Keep_pending=False):
     if self.imagen:
        self.imagen.storage.delete(self.imagen.name)
        super().delete()