from django import forms  
from django.contrib.auth.forms import UserCreationForm
from .models import User
from .models import Tarea

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model=User
        fields=['first_name','email','username','password1','password2']
        
    def __init__(self,*args, **kwargs):
        super(UserCreationForm,self).__init__(*args, **kwargs)
        for name,field in self.fields.items():
            field.widget.attrs.update({'class':'input'})
    
class TareaForm(forms.ModelForm):
    class Meta:
        model = Tarea
        fields = ['titulo', 'imagen', 'descripcion', 'valor']

        #fields = '__all__'