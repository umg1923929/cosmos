from django.contrib import admin
from .models import User, Tarea 

class TareasAdmin(admin.ModelAdmin):
    readonly_fields = ("creado",)

# Register your models here.
admin.site.register(User)
admin.site.register(Tarea, TareasAdmin)