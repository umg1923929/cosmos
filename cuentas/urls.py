from django.urls import path
from . import views
from django.conf import settings
from django.contrib.staticfiles.urls import static

urlpatterns = [
    path('',views.inicio, name="home"), #primera vista al iniciar servidor
    path('signin/',views.signin, name='signin'),
    path('signup/',views.signup, name='signup'),
    path('cerrarsesion',views.signout, name='cerrarsesion'),
    path('profile/cerrarsesion', views.signout, name='cerrarsesion'),
    path('profile/',views.perfil, name='perfil'),
    path('nosotros', views.nosotros, name='nosotros'),
    path('tareas', views.tareas, name='tareas'),
    path('tareas/crear', views.crear, name='crear'),
    path('tareas/editar', views.editar, name='editar'),
    path('eliminar/<int:id>', views.eliminar, name='eliminar'),
    path('tareas/editar/<int:id>',views.editar, name='editar'),
    path('tareacompletada/<int:id>', views.tareacompletada, name='tareacompletada'),
    path('cancelartareacompletada/<int:id>', views.cancelartareacompletada, name='cancelartareacompletada'),
    path('tareasrealizadas', views.tareasrealizadas, name='tareasrealizadas'),
    path('ranking_usuarios', views.ranking_usuarios, name='ranking_usuarios'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
