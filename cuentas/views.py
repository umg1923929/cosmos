#Importar módulos y crear vista de registro
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import HttpResponseRedirect
from django.http import Http404
from .forms import CustomUserCreationForm
from .models import Tarea, User
from .forms import TareaForm
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Q
#from tareas.models import #nombre

# Create your views here.
def inicio(request): 
    return render(request, 'paginas/home.html')

def signup(request):
    #if request.user.is_authenticated:
    #    return redirect('/signin')
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            #login en comentario si no quieres rastrearlo
            login(request, user)
            return redirect('/signin')
        else:
            return render(request, 'customauthentication/signup.html', {'form': form})
    else:
        form = CustomUserCreationForm()
        return render(request, 'customauthentication/signup.html', {'form': form})
    
def signin(request):
    #if request.user.is_authenticated:
    #    return render(request, 'paginas/home.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return render(request, 'paginas/home.html')
            #return redirect('/profile') #profile
        else:
            msg = 'Error Login'
            form = AuthenticationForm(request.POST)
            return render(request, 'customauthentication/signin.html', {'form': form, 'msg': msg})
    else:
        form = AuthenticationForm()
        return render(request, 'customauthentication/signin.html', {'form': form})

@login_required
def signout(request):
    logout(request)
    #return redirect('/home')
    return render(request, 'paginas/home.html')


@login_required
def tareas(request):
    #tareas = Tarea.objects.all()
    '''
    if request.user.is_authenticated:
        tareas = Tarea.objects.filter(usuario=request.user, datecompleted__isnull=True)
    else:
        tareas = Tarea.objects.all()
    '''
    tareas = Tarea.objects.filter(usuario=request.user.id, fechacompletada__isnull=True)    
    #tareas = Tarea.objects.filter(usuario=request.user, datecompleted__isnull=True)
    return render(request, 'tarea/index.html', {'tareas': tareas})

@login_required
def crear(request):
    formulario = TareaForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        # Obtener usuario de la sesión
        usuario = request.user
        # Asignar usuario antes de guardar
        formulario.instance.usuario = usuario   
        formulario.save()
        return redirect('tareas')
    return render(request, 'tarea/crear.html',{'formulario': formulario})

@login_required
def editar(request, id):
    try:
        tarea = Tarea.objects.get(id=id, usuario=request.user)
    except Tarea.DoesNotExist:
        raise Http404("La tarea no existe")
    formulario = TareaForm(request.POST or None, request.FILES or None, instance=tarea)
    #if formulario.is_valid() and request.method == 'POST':  
    if formulario.is_valid() and request.POST: 
        formulario.save()
        return redirect('tareas')
    return render(request, 'tarea/editar.html', {'formulario': formulario})

@login_required
def eliminar(request, id):
    tarea = Tarea.objects.get(id=id)
    tarea.delete()
    return redirect('tareas')

@login_required
def tareacompletada(request, id): 
    tarea = Tarea.objects.get(id=id, usuario=request.user)
    if request.method == 'POST':
        tarea.fechacompletada = timezone.now() 
        tarea.save()
        return redirect('tareas') 
@login_required
def tareasrealizadas(request):
    tareas = Tarea.objects.filter(usuario=request.user.id, fechacompletada__isnull=False).order_by('-fechacompletada')   
    return render(request, 'tarea/index2.html', {'tareas': tareas})

@login_required
def cancelartareacompletada(request, id):
  tarea = Tarea.objects.get(id=id, usuario=request.user)
  if request.method == 'POST':
    tarea.fechacompletada = None
    tarea.save()
    return redirect('tareas')
  
@login_required
def ranking_usuarios(request):
  ranking = User.objects.annotate(
    total_tareas=Count('tarea', filter=Q(tarea__fechacompletada__isnull=False))  
  ).order_by('-total_tareas')
  context = {
    'ranking': ranking
  }
  return render(request, 'tarea/ranking.html', context)



@login_required
def perfil(request): 
    return render(request, 'paginas/profile.html')

@login_required
def nosotros(request):
    return render(request, 'paginas/nosotros.html')